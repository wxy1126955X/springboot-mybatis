package com.example.demo6.controller;

import com.example.demo6.bean.CommonResult;
import com.example.demo6.bean.Department;
import com.example.demo6.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DeptController{

    @Autowired
    DeptService deptService;

    @RequestMapping("/getDeptById/{id}")
    @ResponseBody
    public CommonResult getDeptNameById1(@PathVariable("id") Integer id){
        Department department =deptService.getDeptNameById(id);
        System.out.println(department.toString());
        CommonResult commonResult=new CommonResult();

        if(department !=null){
            commonResult.setCode(200);
            commonResult.setMsg("success");
            commonResult.setObject(department);
        }else {
            commonResult.setCode(400);
            commonResult.setMsg("no result");
        }
        return commonResult;
    }

    @RequestMapping("/getDeptById2/{id}")
    @ResponseBody
    public String getDeptNameById2(@PathVariable("id") Integer id){
        Department department =deptService.getDeptNameById2(id);
        System.out.println(department.toString());
        return "sucess";
    }

}
