package com.example.demo6.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 封装http请求
 * @author 76519
 *
 */
public class HttpClientUtils {

    private static Log log = LogFactory.getLog(HttpClientUtils.class);

    private final static String DEFAULT_ENCODING = "UTF-8";
    private final static int DEFAULT_CONNECT_TIMEOUT = 5000; // 设置连接超时时间，单位毫秒
    private final static int DEFAULT_CONNECTION_REQUEST_TIMEOUT = 1000;// 设置从connect Manager获取Connection 超时时间，单位毫秒
    private final static int DEFAULT_SOCKET_TIMEOUT = 5000;// 请求获取数据的超时时间，单位毫秒 如果访问一个接口，多少时间内无法返回数据，就直接放弃此次调用

    /**
     * 简单http post请求
     * @param url 地址
     * @param paramMap  参数
     * @param encoding  编码
     * @return
     * @throws ParseException
     * @throws IOException
     */
    public static String simplePost(String url, Map<String,String> paramMap, String encoding) throws ParseException, IOException {
        String body = "";
        encoding = StringUtils.isBlank(encoding)?DEFAULT_ENCODING:encoding;
        //1、创建CloseableHttpClient对象
        CloseableHttpClient client = HttpClients.createDefault();
        //2、创建请求方法的实例，并指定请求URL。如果需要发送GET请求，创建HttpGet对象；如果需要发送POST请求，创建HttpPost对象。
        HttpPost httpPost = postForm(paramMap,url,DEFAULT_ENCODING);
        //执行请求操作，并拿到结果（同步阻塞）
        CloseableHttpResponse response = null;
        try {
            //5、调用CloseableHttpClient对象的execute(HttpUriRequest request)发送请求，该方法返回一个CloseableHttpResponse。
            response = client.execute(httpPost);
            //6、调用HttpResponse的getEntity()方法可获取HttpEntity对象，该对象包装了服务器的响应内容。程序可通过该对象获取服务器的响应内容；调用CloseableHttpResponse的getAllHeaders()、getHeaders(String name)等方法可获取服务器的响应头。
            StatusLine status = response.getStatusLine();
            log.info("请求回调状态               ："+status);
            //获取结果实体
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                //按指定编码转换结果实体为String类型
                body = EntityUtils.toString(entity, encoding);
                log.info("请求回调数据               ："+body);
            }
            //7、释放连接。无论执行方法是否成功，都必须释放连接
            EntityUtils.consume(entity);
        }catch (UnsupportedEncodingException e){
            e.printStackTrace();
            log.error("简单post请求遇到UnSpEcode异常",e);
            throw new IOException(e);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("简单post请求遇到IO异常",e);
            throw new IOException(e);
        } catch (ParseException e) {
            e.printStackTrace();
            log.error("简单post请求遇到PE异常",e);
            throw new ParseException();
        } finally{
            //释放链接
            response.close();
        }
        return body;
    }

    /**
     * post请求url与请求参数组装
     * @param paramMap
     * @param url
     * @param encoding
     * @return
     * @throws UnsupportedEncodingException
     */
    private static HttpPost postForm(Map<String,String> paramMap,String url,String encoding) throws UnsupportedEncodingException {
        HttpPost httpPost = new HttpPost(url);
        //装填参数
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        if(paramMap!=null){
            for (Map.Entry<String, String> entry : paramMap.entrySet()) {
                nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
        }
        //3、如果需要发送请求参数，可可调用setEntity(HttpEntity entity)方法来设置请求参数。setParams方法已过时（4.4.1版本）
        httpPost.setEntity(new UrlEncodedFormEntity(nvps, encoding));
        //4、调用HttpGet、HttpPost对象的setHeader(String name, String value)方法设置header信息，或者调用setHeaders(Header[] headers)设置一组header信息。
        httpPost.setHeader("Content-type", "application/x-www-form-urlencoded");
        httpPost.setHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(DEFAULT_CONNECT_TIMEOUT).setConnectionRequestTimeout(DEFAULT_CONNECTION_REQUEST_TIMEOUT)
                .setSocketTimeout(DEFAULT_SOCKET_TIMEOUT).build();
        httpPost.setConfig(requestConfig);
        return httpPost;
    }
    /**
     * 简单get请求传输
     * @param url
     * @param paramMap
     * @param encoding
     * @return
     */
    public static String simpleGet(String url, Map<String,String> paramMap, String encoding){
        String body = "";
        encoding = StringUtils.isBlank(encoding)?DEFAULT_ENCODING:encoding;
        //1、创建CloseableHttpClient对象
        CloseableHttpClient client = HttpClients.createDefault();

        //2、创建get请求
        HttpGet httpGet = new HttpGet(url);
        //装填参数
        List<NameValuePair> lists = new ArrayList<NameValuePair>();
        if(paramMap != null){
            //每个key-value构成一个entrySet对象
            Set<Map.Entry<String, String>> setMap = paramMap.entrySet();
            //遍历对象  将值保存list集合种
            for (Map.Entry<String, String> entry:setMap) {
                lists.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
        }
        //将传递参数  编码化
        String param = URLEncodedUtils.format(lists, encoding);
        log.info("simpleGet------->"+param);
        //设置数据
        httpGet.setURI(URI.create(url+"?"+param));
        log.info("simpleGet --- url------->"+httpGet.getURI().toString());
        //配置连接参数信息
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(DEFAULT_CONNECT_TIMEOUT).setConnectionRequestTimeout(DEFAULT_CONNECTION_REQUEST_TIMEOUT)
                .setSocketTimeout(DEFAULT_SOCKET_TIMEOUT).build();
        httpGet.setConfig(requestConfig);

        //执行请求操作，并拿到结果（同步阻塞）
        CloseableHttpResponse response = null;
        try {
            //5、调用CloseableHttpClient对象的execute(HttpUriRequest request)发送请求，该方法返回一个CloseableHttpResponse。
            response = client.execute(httpGet);
            //6、调用HttpResponse的getEntity()方法可获取HttpEntity对象，该对象包装了服务器的响应内容。程序可通过该对象获取服务器的响应内容；调用CloseableHttpResponse的getAllHeaders()、getHeaders(String name)等方法可获取服务器的响应头。
            StatusLine status = response.getStatusLine();
            log.info("get请求回调状态               ："+status);
            //获取结果实体
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                //按指定编码转换结果实体为String类型
                body = EntityUtils.toString(entity, encoding);
                log.info("get请求回调数据               ："+body);
            }
            //7、释放连接。无论执行方法是否成功，都必须释放连接
            EntityUtils.consume(entity);
        }catch (UnsupportedEncodingException e){
            e.printStackTrace();
            log.error("简单get请求遇到UnSpEcode异常",e);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("简单get请求遇到IO异常",e);
        } catch (ParseException e) {
            e.printStackTrace();
            log.error("简单get请求遇到PE异常",e);
            throw new ParseException();
        } finally{
            //释放链接
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return body;
    }
}