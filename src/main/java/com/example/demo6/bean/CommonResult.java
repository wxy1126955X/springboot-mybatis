package com.example.demo6.bean;

public class CommonResult {
    private  Integer code; //200 sucess 500语法错误
    private String msg; //
    private  Object object;//封装对象
    private  String token;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public CommonResult(Integer code, String msg, Object object) {
        this.code = code;
        this.msg = msg;
        this.object = object;
    }

    public CommonResult() {
    }
}
