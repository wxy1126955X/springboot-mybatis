package com.example.demo6.service;


import com.example.demo6.bean.Department;
import com.github.pagehelper.PageInfo;
import com.jx.social.data.platform.core.base.PageQuery;

import java.util.List;
import java.util.Map;

public interface DeptService {
    Department getDeptNameById(Integer id);

    Department getDeptNameById2(Integer id);

    PageInfo<Department> queryPage(PageQuery pageQuery, Map<String, Object> dataMap);

    PageInfo<Department> queryPage2(PageQuery pageQuery, Map<String, Object> dataMap);

    List<Department> queryList();
}
