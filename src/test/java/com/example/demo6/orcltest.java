package com.example.demo6;


import java.sql.*;

/**
 * @Author wang
 * @Date 2022/3/25 14:19
 */


public class orcltest {
    public static void main(String[] args) {
        ResultSet rs = null;
        PreparedStatement stmt = null;
        Connection conn = null;
        try {
            //加载驱动和地址，及地址连接
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String dbURL = "jdbc:oracle:thin:@//10.35.168.154:1521/mulufrn";
            //我使用的是oracle12c，这里是没有scott用户的，我自己新建的用户c##scott
            conn = DriverManager.getConnection(dbURL, "tc_gaj", "gaj8614");

            //查询
            String rno = "181";
            String sql = "select * from RF_HOUSE where ROOMNO = "+rno;
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet res = ps.executeQuery();
            while (res.next()){
                System.out.println(res.getString("HOUSECODE"));
            }
            System.out.println("数据库连接成功！");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("数据库连接失败！");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}


