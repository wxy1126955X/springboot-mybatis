package com.example.demo6.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author wang
 * @Date 2022/3/1 18:44
 */

public class GetUsersAttenDancereCord {
    //static DBSession dbSession = DBSessionFactory.createSession();
    private static String AppKey =  "ww3f53ca2b16b4c600";//单位企业微信id
    private static String AppSecre = "dcLNLEQ6XMEfLCmOKGibZGp48CWzHq-kkosHvxisGtg";//通讯录 AppSecre 值
    private static String Corpsecret ="GSJInKomQ-SFcKSuwQm4lKMeLyKTqDB1xNy0e2wUVT8";//打卡 AppSecre 值
    private static String starttime  = "2022-3-1 00:00:00";
    private static String endtime = "2022-3-4 16:00:00";

    public static void main(String[] args) throws Exception {
        //getAccToken();
        //System.out.println(getAccToken());
        //获取某个应用的token
        //根据token和请求参数获取数据

        /*
        List<String> userlist = new ArrayList<>();
        userlist.add("wf");
        getDKData("","",userlist,getAccToken());*/
        Date2TimeStamp("2022-3-1 00:00:00","yyyy-MM-dd HH:mm:ss");
        Date2TimeStamp("2022-3-2 16:00:00","yyyy-MM-dd HH:mm:ss");
        //企业通讯录token
        String tongxunluToken  = getAccToken(AppSecre);
        getAllQiYeUser(starttime,endtime,tongxunluToken);
    }

    public static String getAccToken(String secret) throws IOException {
        //当前时间戳
        String generalUrl = "https://qyapi.weixin.qq.com/cgi-bin/gettoken"; //Token地址
        URL url = new URL(generalUrl);
        String strRead = null;
        StringBuffer sbf = new StringBuffer();
        // 连接接口路径
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        // 设置请求属性
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setUseCaches(false);
        connection.setDoOutput(true);
        connection.setDoInput(true);
        // 建立连接
        connection.connect();
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
        // 插入Body里面的json数据
        JSONObject parm = new JSONObject();
        parm.put("corpid", AppKey);  //AppKey  单位id
        parm.put("corpsecret", secret);//AppSecre  应用corpsecret值
        writer.write(parm.toString());
        writer.flush();
        InputStream is = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        while ((strRead = reader.readLine()) != null) {
            sbf.append(strRead);
            sbf.append("\r\n");
        }
        reader.close(); //关流
        connection.disconnect();
        String ss = JSONObject.parseObject(sbf.toString()).getString("access_token"); //获取data对象
        if ("".equals(ss) || ss == null) {
            System.out.println("获取token失败>>>>>ss的值是：" + ss);
        }
        return ss;  //返回token字符串
    }

    /**
     * 获取组织架构id和人员Userid
     * @return
     */
    private static void getAllQiYeUser(String starttime, String endtime, String token) throws Exception {
        List<Object> users = new ArrayList<>();
        //--拉取企业微信所有部门信息
        String generalUrl = "https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token="+token+"&department_id=1&fetch_child=1"; //Token地址
        URL url = new URL(generalUrl);
        String strRead = null;
        StringBuffer sbf = new StringBuffer();
        // 连接接口路径
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        // 设置请求属性
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setUseCaches(false);
        connection.setDoOutput(true);
        connection.setDoInput(true);
        // 建立连接
        connection.connect();
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
        // 插入Body里面的json数据
        JSONObject parm = new JSONObject();
        writer.write(parm.toString());
        writer.flush();
        InputStream is = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        while ((strRead = reader.readLine()) != null) {
            sbf.append(strRead);
            sbf.append("\r\n");
        }
        reader.close(); //关流
        connection.disconnect();
        JSONObject object = JSONObject.parseObject(sbf.toString());
        //System.out.print(object);
        //JSONArray jsonArray = new JSONArray(object.get("userlist").toString());
        JSONArray jsonArray = JSONObject.parseArray(object.get("userlist").toString());
        int iSize = jsonArray.size();
        List<String> userlist = new ArrayList<String>();//将员工Userid放到List里面
        for (int i = 0; i < iSize; i++) {
            //org.json.JSONObject jsonObject = jsonArray.getJSONObject(i);
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String userid = JSON.toJSONString(jsonObject.get("userid"));//转换String的方法 CUtil.convert2String();
            userlist.add(userid);
        }

        List<List<String>> list = fixedGrouping2(userlist, 100); //因为企业微信API一次性只能获取固定人员和30天的数据因此将人员进行拆分

        for (List<String> list2 : list) {
            getDKData(starttime, endtime, list2,getAccToken(Corpsecret));
        }

    }

    /**
     * 将一组数据固定分组，每组n个元素
     *
     * @param source 要分组的数据源
     * @param n      每组n个元素
     * @param <T>
     * @return
     */
    public static <T> List<List<T>> fixedGrouping2(List<T> source, int n) {

        if (null == source || source.size() == 0 || n <= 0)
            return new ArrayList<List<T>>();
        List<List<T>> result = new ArrayList<List<T>>();
        int remainder = source.size() % n;
        int size = (source.size() / n);
        for (int i = 0; i < size; i++) {
            List<T> subset = null;
            subset = source.subList(i * n, (i + 1) * n);
            result.add(subset);
        }
        if (remainder > 0) {
            List<T> subset = null;
            subset = source.subList(size * n, size * n + remainder);
            result.add(subset);
        }
        return result;
    }

    public  static  void getDKData(String starttime,String endtime,List userlist,String token) throws Exception{
        System.out.print(userlist);
        //long kssj = CUtil.convert2Date(starttime).getTime()/1000; //开始时间
        //long jssj = CUtil.convert2Date(endtime).getTime()/1000; //结束时间
        long kssj = Date2TimeStamp("2022-3-1 00:00:00" ,"yyyy-MM-dd HH:mm:ss");
        long jssj = Date2TimeStamp("2022-3-2 18:00:00" ,"yyyy-MM-dd HH:mm:ss");
        String generalUrl = "https://qyapi.weixin.qq.com/cgi-bin/checkin/getcheckindata?access_token="+token+""; //Token地址
        URL url = new URL(generalUrl);
        String strRead = null;
        StringBuffer sbf = new StringBuffer();
        // 连接接口路径
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        // 设置请求属性
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setUseCaches(false);
        connection.setDoOutput(true);
        connection.setDoInput(true);
        // 建立连接
        connection.connect();
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
        // 插入Body里面的json数据
        JSONObject parm = new JSONObject();
        parm.put("opencheckindatatype","3");    //打卡类型。1：上下班打卡；2：外出打卡；3：全部打卡
        parm.put("starttime", kssj);
        parm.put("endtime", jssj);
        parm.put("useridlist", userlist);
        System.out.println("body参数"+parm.toString());
        writer.write(parm.toString());
        writer.flush();
        InputStream is = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        while ((strRead = reader.readLine()) != null) {
            sbf.append(strRead);
            sbf.append("\r\n");
        }
        reader.close(); //关流
        connection.disconnect();

        JSONObject object = JSONObject.parseObject(sbf.toString());
        System.out.print(object);

        //JSONArray jsonArray = new JSONArray(object.get("checkindata").toString());//获取打卡数据
        JSONArray jsonArray = JSONObject.parseArray(object.get("checkindata").toString());
        int iSize = jsonArray.size();
        /*for (int i = 0; i < iSize; i++) {//循环验证状态
            org.json.JSONObject jsonObject = jsonArray.getJSONObject(i);
            String deviceid = CUtil.convert2String(jsonObject.get("deviceid"));//获取打卡设备id。如果有值则证明打卡了。如果无则证明没有
            String exception_type = CUtil.convert2String(jsonObject.get("exception_type"));//打卡类型 未打卡状态为：未打卡，此处为空的情况为走了补考勤申请因此需要进行两个条件来验证
            if (deviceid.length() > 0 || exception_type.length() == 0) {
                insterIorecordSync(jsonObject);//进行数据同步操作
            }
        }*/

    }

    /**
     * 日期格式字符串转换成时间戳
     *
     * @param dateStr 字符串日期
     * @param format   如：yyyy-MM-dd HH:mm:ss
     *
     * @return
     */
    public static long Date2TimeStamp(String dateStr, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            System.out.println(dateStr+">>>>>"+sdf.parse(dateStr).getTime() / 1000);
            return sdf.parse(dateStr).getTime() / 1000;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
