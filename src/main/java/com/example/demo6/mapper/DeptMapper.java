package com.example.demo6.mapper;


import com.example.demo6.bean.Department;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

@Mapper
public interface DeptMapper extends BaseMapper<Department> {
    @Select("select * from department where id = #{id}")
    Department getDeptById(Integer id);

    @Select("select * from department")
    List<Department> queryList();

    @Select("insert department values('123','456')")
    void insert();
}
