package com.example.demo6.controller;


import com.example.demo6.bean.Department;
import com.example.demo6.mapper.DeptMapper;
import com.example.demo6.service.DeptService;
import com.github.pagehelper.PageInfo;
import com.jx.social.data.platform.core.base.BaseResponseEntity;
import com.jx.social.data.platform.core.base.PageQuery;
import com.jx.social.data.platform.core.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author wang
 * @Date 2022/3/31 21:51
 */
@RestController
public class TestController {
    @Autowired
    private DeptService deptService;

    @Autowired
    private DeptMapper deptMapper;
    @PostMapping("bodyParm")
    public Map<String,String> bodyParmTest(@RequestBody Map<String,Object> map){
        Map<String,String> map1 = new HashMap<>();
        List<String> list = (List<String>) map.get("comId");
        List<Map<String,String>> list1 = (List<Map<String, String>>) map.get("label");
        return map1;
    }

    @RequestMapping("queryPage")
    public BaseResponseEntity CompanyLabelList(HttpServletRequest request, @RequestBody Map<String, Object> dataMap){


        PageQuery pageQuery= PageUtils.getPageQuery(request);
        PageInfo<Department> list =deptService.queryPage(pageQuery, dataMap);

        return BaseResponseEntity.OK(list);

    }
    @RequestMapping("queryPage2")
    public BaseResponseEntity CompanyLabelList2(HttpServletRequest request, @RequestBody Map<String, Object> dataMap){

        PageQuery pageQuery= PageUtils.getPageQuery(request);
        PageInfo<Department> list =deptService.queryPage2(pageQuery, dataMap);

        return BaseResponseEntity.OK(list);

    }

    //事务测试
    @RequestMapping("trans")
    public BaseResponseEntity trans(HttpServletRequest request, @RequestBody Map<String, Object> dataMap){

        deptMapper.insert();

        return BaseResponseEntity.OK();

    }



}
