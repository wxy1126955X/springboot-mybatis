package com.example.demo6.service;


import com.example.demo6.bean.Department;
import com.example.demo6.mapper.DeptMapper;
import com.github.pagehelper.PageInfo;
import com.jx.social.data.platform.core.base.PageQuery;
import com.jx.social.data.platform.core.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class DeptServiceImpl implements DeptService {
    @Autowired
    DeptMapper deptMapper;

    @Autowired
    DeptService deptService;

    @Override
    public Department getDeptNameById(Integer id) {

        return deptMapper.getDeptById(id);
    }

    @Override
    public Department getDeptNameById2(Integer id) {
        Department department = new Department();
        department.setId(id);

        return deptMapper.selectById(id);
    }

    @Override
    public PageInfo<Department> queryPage(PageQuery pageQuery, Map<String, Object> dataMap) {
        List<String> list =new ArrayList<>();
        for (int i = 0; i <10 ; i++) {
            list.add(String.valueOf(i));
        }
        return PageUtils.setPageHelper(pageQuery, () ->deptMapper.queryList());
    }
    @Override
    public PageInfo<Department> queryPage2(PageQuery pageQuery, Map<String, Object> dataMap) {
        List<String> list =new ArrayList<>();
        for (int i = 0; i <10 ; i++) {
            list.add(String.valueOf(i));
        }
        return PageUtils.setPageHelper(pageQuery, () ->queryList());
    }
    @Override
    public List<Department> queryList() {
        List<Department> strings = new ArrayList<>();
        for (int i = 0; i <50 ; i++) {
            Department department = new Department();
            department.setId(1);
            strings.add(department);
        }
        return strings;
    }


}
