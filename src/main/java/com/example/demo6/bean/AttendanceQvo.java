package com.example.demo6.bean;

import lombok.Data;

import java.io.Serializable;

@Data
public class AttendanceQvo implements Serializable {
    private String checkType;     //上下午
    private String userId;        //员工ID
    private long userCheckTime;//实际打卡时间
    private long workDate;      //工作日

}