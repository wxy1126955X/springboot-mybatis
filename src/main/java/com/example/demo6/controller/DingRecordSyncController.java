package com.example.demo6.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo6.bean.AttendanceQvo;
import com.example.demo6.util.HttpClientUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.apache.http.HttpResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DingRecordSyncController {

    public static void main(String[] args) throws IOException {
        List<AttendanceQvo> users = new ArrayList<>();
        Date date = new Date();

        //3月7号到3月8号的考勤记录
        String workDateFrom = "2022-4-21" + " " + "06:00:00";

        String workDateTo = "2022-4-22" + " " + "06:00:00";

        /*String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        String yesterday = getPreDayOrAfterDay(currentDate, -43);
        String workDateFrom = yesterday + " " + "06:00:00";
        //打卡结束时间
        String workDateTo = getPreDayOrAfterDay(currentDate, -40) + " " + "06:00:00";*/

        //String workDateTo = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);//当前时间

        String access_token_str = getAccessToken();//获取access_token
        //String access_token_str = "207404d3d5593930b9193184fad77ec0";

        JSONObject jsonStr = JSONObject.parseObject(access_token_str); //转json结构
        String access_Token = jsonStr.getString("access_token");//获取access_Token
        getAttendances(users, workDateFrom,workDateTo, access_Token);//凌晨的
        System.out.println("获取从昨天六点到当前时间的打卡记录"+ Arrays.asList(users));


        //获取当前企业所有部门id

        getDepertmentId(access_Token,"");
        System.out.println(getDepertmentId(access_Token,"").toString());

    }

    private static String accessTokenUrl = "https://oapi.dingtalk.com/gettoken";//通用
    private static String corpId = "dingljnq2oisgkpgtic6";
    private static String corpSecret = "nr3gSuIToj4ek3GGg1kcBWtAeILyXZpO8vvBRn-RRml588lYo6FkWxsSZAVTDZkD";

    public static String getAccessToken() {
        Map<String,Object> map = new HashMap<>();
        map.put("corpid",corpId);
        map.put("corpsecret",corpSecret);
        return httpGetStringResult(accessTokenUrl, map);//获取access_token
    }

    public static String httpGetStringResult(String url,Map<String,Object> param){
        String content = null;
        CloseableHttpClient httpClient = HttpClients.createDefault();
        if(param != null && !param.isEmpty()){
            StringBuffer strparams = new StringBuffer();
            for (Map.Entry<String, Object> map : param.entrySet()) {
                strparams.append(map.getKey()).append("=").append(map.getValue().toString()).append("&");
            }
            strparams = strparams.deleteCharAt(strparams.length()-1);
            url = url + "?" + strparams;
        }
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response = null;

        try {
            response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            content = EntityUtils.toString(entity,"UTF-8");
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }finally {
            try {
                if(null!=response){
                    response.close();
                }
            } catch (IOException e) {

                e.printStackTrace();
            }
        }

        return content;
    }

    public static String doPost(String requestUrl,JSONObject json){
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(requestUrl);
        post.setHeader("Content-Type", "application/json");
        post.addHeader("Authorization", "Basic YWRtaW46");
        String result = "";
        try {
            StringEntity s = new StringEntity(json.toString(), "utf-8");
            s.setContentEncoding(new BasicHeader("contentType",
                    "application/json"));
            post.setEntity(s);
            // 发送请求
            HttpResponse httpResponse = client.execute(post);
            // 获取响应输入流
            InputStream inStream = httpResponse.getEntity().getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inStream, "utf-8"));
            StringBuilder strber = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null)
                strber.append(line + "\n");
            inStream.close();
            result = strber.toString();
            System.out.println(result);
            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {

                System.out.println("请求服务器成功，做相应处理");

            } else {
                System.out.println("请求服务端失败");
            }
        } catch (Exception e) {
            System.out.println("请求异常");
            throw new RuntimeException(e);
        }
        return result;
    }

    public static List<AttendanceQvo> getAttendances(List<AttendanceQvo> users, String workDateFrom, String workDateTo, String access_Token) {
        List list = Arrays.asList("wangxueyi","manager368","。。。自己公司的员工钉钉ID");
        int listSize=list.size();
        int toIndex = 50;
        for(int i = 0;i < list.size();i += 50){
            if(i+50 > listSize){        //作用为toIndex最后没有50条数据则剩余几条newList中就装几条
                toIndex = listSize-i;
            }
            List newList = list.subList(i,i + toIndex);
            Boolean hasMore = true;
            int offset = 0;//为了分页
            do{
                Map<String,Object> mapParam = new HashMap<>();
                mapParam.put("workDateFrom", workDateFrom);
                mapParam.put("workDateTo", workDateTo);
                mapParam.put("userIdList", newList);
                mapParam.put("offset", offset * 50);
                mapParam.put("limit", 50);
                String attendanceStr = getAttendance(mapParam, access_Token);
                JSONObject firstJson = JSONObject.parseObject(attendanceStr);

                hasMore = firstJson.getBoolean("hasMore");
                JSONArray recordFirst = firstJson.getJSONArray("recordresult");//当前部门下的userList
                for(int j = 0;j < recordFirst.size(); j++) {
                    JSONObject record = recordFirst.getJSONObject(j);
                    AttendanceQvo attendanceQvo = new AttendanceQvo();
                    attendanceQvo.setCheckType(record.getString("checkType"));
                    attendanceQvo.setUserId(record.getString("userId"));
                    attendanceQvo.setWorkDate(record.getLong("workDate"));
                    attendanceQvo.setUserCheckTime(record.getLong("userCheckTime"));
                    users.add(attendanceQvo);
                }
                if(hasMore) {//有下一页偏移量加一
                    offset++;
                }
            } while (hasMore);
        }
        return users;
    }
    public static String getAttendance(Map<String, Object> map ,String access_token_str) {
        String dingDingAttendance = "https://oapi.dingtalk.com/attendance/list?access_token="+access_token_str;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("workDateFrom",map.get("workDateFrom"));
        jsonObject.put("workDateTo",map.get("workDateTo"));
        jsonObject.put("limit",map.get("limit"));
        jsonObject.put("offset",map.get("offset"));
        jsonObject.put("userIdList",map.get("userIdList"));
        return doPost(dingDingAttendance,jsonObject);//获取考勤记录

    }

    /* * @Description: -1是前一天， +1是后一天
     * @Date: 17:28 2018/5/29
     */
    public static String getPreDayOrAfterDay(String current, int flag) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();//获取日历实例
        try {
            calendar.setTime(sdf.parse(current));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.add(Calendar.DAY_OF_MONTH, flag);  //设置为前一天
        String yesterday = sdf.format(calendar.getTime());//获得前一天
        return yesterday;
    }



    //获取所有部门id
    public static List<String> getDepertmentId(String accessToken, String id) throws IOException {
        List<String> depertmentIdList = new ArrayList<>();
        String url = "";
        if (id != null && !id.isEmpty()) {
            url = "https://oapi.dingtalk.com/topapi/v2" +
                    "/department/listsubid?access_token=" + accessToken + "&id=" + id;
        } else {
            url = "https://oapi.dingtalk.com/topapi/v2" +
                    "/department/listsubid?access_token=" + accessToken + "&dept_id=" + 1;
        }
        JSONObject response = JSONObject.parseObject
                (HttpClientUtils.simplePost(url,new HashMap<>(),"UTF-8"));
        JSONObject object = response.getJSONObject("result");

        JSONArray dept_id_list = object.getJSONArray("dept_id_list");

        for (int i=0; i<dept_id_list.size(); i++) {
            String depStr  = String.valueOf(dept_id_list.get(i)) ;
            depertmentIdList.add(depStr);
        }
        return depertmentIdList;
    }



    //获取部门员工
    /*public static List<OapiV2UserListResponse.ListUserResponse> getUserByDeptId(Long deptId, Long offset, Long size) {

        try {
            DingTalkClient client = new DefaultDingTalkClient(DingTalkConstant.BASE_URL + "/topapi/v2/user/list");
            OapiV2UserListRequest req = new OapiV2UserListRequest();
            req.setDeptId(deptId);
            req.setCursor(offset * size);
            req.setSize(size);
            req.setOrderField("modify_desc");
            req.setContainAccessLimit(false);
            req.setLanguage("zh_CN");
            OapiV2UserListResponse rsp = client.execute(req, BaseDingTalk.getAccessToken());

            if (!rsp.isSuccess()) {
                return null;
            }

            List<OapiV2UserListResponse.ListUserResponse> list = rsp.getResult().getList();

            if (rsp.getResult().getHasMore()) {
                list.addAll(getUserByDeptId(deptId, offset++, size));
            }

            return list;

        } catch (Exception e) {
            log.error("[DingTalk] 获取部门用户失败", e);
        }

        return null;
    }*/

    //获取部门员工 名字 id

    //获取部门员工列表
    /*public static List<String> getUserIdListByDeptId(String depId,String accessToken){


    }*/




}